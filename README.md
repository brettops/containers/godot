# **godot** container

<!-- BADGIE TIME -->

[![brettops pipeline](https://img.shields.io/badge/brettops-pipeline-209cdf?labelColor=162d50)](https://brettops.io)
[![pipeline status](https://gitlab.com/brettops/pipelines/container/badges/main/pipeline.svg)](https://gitlab.com/brettops/pipelines/container/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

<!-- END BADGIE TIME -->

Container for building [Godot](https://godotengine.org/) games in CI.

View the examples:

- [Godot 3.5 HTML5 export](https://brettops.gitlab.io/containers/godot/3.5/)

- [Godot 4.1 HTML5 export](https://brettops.gitlab.io/containers/godot/4.1/)
