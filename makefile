IMAGE ?= godot

.PHONY: all build clean run

all: build run

build:
	docker build -t $(IMAGE) .

run:
	docker run --rm -ti $(IMAGE)

dev:
	docker run --rm -it -v `pwd`:/code -w /code $(IMAGE) bash

clean:
	rm -f *.zip *.tpz Godot_*
