FROM ${CONTAINER_PROXY}ubuntu:22.04

# hadolint ignore=DL3008
RUN apt-get update -y \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        libfontconfig1 \
        unzip \
        zip \
    && rm -rf /var/lib/apt/lists/*

ARG GODOT_VERSION
ENV GODOT_VERSION="${GODOT_VERSION}"

SHELL ["/bin/bash", "-euo", "pipefail", "-c"]

RUN if [[ "$GODOT_VERSION" =~ ^4 ]] ; then \
        DOWNLOAD_URL="https://github.com/godotengine/godot/releases/download/${GODOT_VERSION}-stable/Godot_v${GODOT_VERSION}-stable_linux.x86_64.zip" ; \
    else \
        DOWNLOAD_URL="https://github.com/godotengine/godot/releases/download/${GODOT_VERSION}-stable/Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip" ; \
    fi ; \
    echo $DOWNLOAD_URL \
    && curl -sSL -o godot.zip "$DOWNLOAD_URL" \
    && unzip godot.zip \
    && install Godot* /usr/local/bin/godot \
    && rm -f godot.zip Godot* \
    && type -p godot

RUN curl -sSL -o export_templates.zip "https://github.com/godotengine/godot/releases/download/${GODOT_VERSION}-stable/Godot_v${GODOT_VERSION}-stable_export_templates.tpz" \
    && mkdir -p /usr/share/godot/templates/ \
    && unzip -d /usr/share/godot/templates/ export_templates.zip \
    && rm -f export_templates.zip \
    && mv -v /usr/share/godot/templates/templates /usr/share/godot/templates/"${GODOT_VERSION}.stable" \
    && mkdir -p ~/.local/share/godot/templates \
    && ln -sf "/usr/share/godot/templates/${GODOT_VERSION}.stable" ~/.local/share/godot/templates/"${GODOT_VERSION}.stable"

CMD ["/bin/bash"]
